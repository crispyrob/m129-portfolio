# Dokumentation Labor 5 - Labor mit zwei Router und DHCP Server

 - Datum: 04.02.2022
 - Name: Robel Aysheshim
 - [Link zur Aufgabenstellung](https://gitlab.com/crispyrob/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)



## Umgebung
![Umgebung](Images/Labor5_umgebung.png)

## Als Admin im CMD eingeben
```
route -p ADD 192.168.33.0 MASK 255.255.255.0 192.168.23.20
route -p ADD 192.168.34.0 MASK 255.255.255.0 192.168.23.21
```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
dhcp
```
## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
dhcp
```
## Config VPC 3
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
dhcp
```
## Config Mikrotik Router r1
```
/interface bridge
add name=bridge_vlan

/interface bridge port
add bridge=bridge_vlan interface=ether1
add bridge=bridge_vlan interface=ether2

/interface vlan
add interface=bridge_vlan name=vlan1 vlan-id=1

/ip pool
add name=dhcp_pool0 ranges=192.168.33.10-192.168.33.254

/ip dhcp-server
add address-pool=dhcp_pool0 interface=bridge_vlan name=dhcp1

/ip dhcp-server network
add address=192.168.33.0/24 gateway=192.168.33.1

/ip address
add address=192.168.34.1/24 interface=bridge_vlan network=192.168.34.0
add address=192.168.255.1/30 interface=ether4 network=192.168.255.0
add address=192.168.23.20/24 interface=ether3 network=192.168.23.0

/ip firewall address-list
add address=192.168.23.0/24 list=diable_ping_23

/ip firewall filter
add action=drop chain=forward dst-address=192.168.33.0/24 protocol=icmp src-address=192.168.23.0/24

/ip route
add dst-address=192.168.34.0/24 gateway=192.168.255.2
```
## Konfiguration Mikrotik Router r2
```
/interface bridge
add name=bridge_vlan

/interface bridge port
add bridge=bridge_vlan interface=ether4
add bridge=bridge_vlan interface=ether3

/interface vlan
add interface=bridge_vlan name=vlan1 vlan-id=1

/ip pool
add name=dhcp_pool0 ranges=192.168.34.10-192.168.34.254

/ip dhcp-server
add address-pool=dhcp_pool0 interface=bridge_vlan name=dhcp1

/ip dhcp-server network
add address=192.168.34.0/24 gateway=192.168.34.1

/ip address
add address=192.168.33.1/24 interface=bridge_vlan network=192.168.33.0
add address=192.168.255.2/30 interface=ether2 network=192.168.255.0
add address=192.168.23.21/24 interface=ether1 network=192.168.23.0

/ip route
add dst-address=192.168.33.0/24 gateway=192.168.255.1
```
## Neue Lerninhalte
- IP Route 
- Konfiguration Mikrotik Router
-Arbeiten mit mehreren Subnetzen

## Reflexion
Ich hatte mit dieser Aufgabe eher Schwierigkeiten, da ich von Anfang an nur mit Cisco gearbeitet habe. Mit etwas Hilfe von meinen Klassenkameraden war aber auch das bald nicht mehr so schwer. Ich habe jedoch einiges dazu lernen können.
