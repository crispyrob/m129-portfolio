# Dokumentation Labor 4 - Aggregierte statische Routen

 - Datum: 04.02.2022
 - Name: Robel Aysheshim
 - [Link zur Aufgabenstellung](https://gitlab.com/crispyrob/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)



## Umgebung
![Umgebung](Images/Umgebung_Labor4.png)

## Als Admin im CMD eingeben
```
route -p ADD 192.168.0.0 MASK 255.255.0.0 192.168.23.24
```

## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] [Gateway]
ip 192.168.11.5 255.255.255.0 192.168.11.1
```
## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC2
# [IP] [MASKE] [Gateway]
ip 192.168.129.5 255.255.255.0 192.168.129.1
```
## Config VPC 3
```
set pcname PC3
# [IP] [MASK] [Gateway]
ip 192.168.193.5 255.255.255.0 192.168.193.1
```
## Config Cisco Router R1
```
config t
    int f0/0
                ip add 192.168.11.1 255.255.255.0
                no shut
        exit
            int f1/0
                ip add 192.168.23.24 255.255.255.0 
                    no shut
        exit
            int f2/0
                ip add 192.168.255.1 255.255
               exit
            ip route 192.168.129.0 255.255.255.0 192.168.255.2
    exit
```
## Konfiguration Cisco Router R2
```
config t
    int f0/0
        ip add 192.168.21.1 255.255.255.0
        no shut
      exit
    int f1/0
        ip add 192.168.255.2 255.255.255.252
        no shut
      exit
    int f2/0 
        ip add 192.168.255..5 255.255.255.252 
        no shut
       exit
     ip route 192.168.11.0 255.255.255.0 192.168.255.1
     ip route 192.168.23.0 255.255.255.0 192.168.255.1
     ip route 192.168.193.0 255.255.255.0 192.168.255.6

exit
```
## Konfiguration Cisco Router R3
```
config t
    int f1/0
        ip add 192.168.193.1 255.255.255.0 
        no shut
       exit
    int f0/0
        ip add 192.168.255.6 255.255.255.252
        no shut
       exit 
    ip route 192.168.0.0 255.255.0.0 192.168.255.5
exit
```
## Neue Lerninhalte
- IP Route 
- Konfiguration Cisco Router 
-Arbeiten mit mehreren Subnetzen

## Reflexion
Diese Aufgabe hat mir sehr viel Freude bereitet. Es ging sehr gut durch. Vorallems gefiel mir noch herauszufinden mit dem IP Route zu arbeiten. Ich konnte viel neues Lernen durch Praktische Arbeit.
