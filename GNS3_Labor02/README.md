# Dokumentation Labor 2 - Ping mit Router

 - Datum: 15.12.2021
 - Name: Robel Aysheshim
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)



## Umgebung
![Umgebung](Images/Umgebung_Labor2.png)


## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x fastethernet 1/0
```
set pcname PC1
# [IP] [MASKE] [Gateway]
ip 192.168.2.2 255.255.255.0 192.168.2.1
subnetz: 192.168.2.0/24
```
## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x fastethernet 0/0
```
set pcname PC1
# [IP] [MASKE] [Gateway]
ip 192.168.1.2 255.255.255.0 192.168.1.1
subnetz: 192.168.1.0/24
```
## Configuration Cisco Router
```
Configure Terminal
    interface fastethernet 1/0
            ip add 192.168.2.1 255.255.255.0
        no shut
    exit
    interface fastethernet0/0
            ip add 192.168.1.1 255.255.255.0
        no shut
    exit
exit
``` 

## Neue Lerninhalte
- Konfiguration eines Cisco Routers

## Reflexion
Durch diese Übung konnte ich lernen wie man einen Router Konfiguriert und Computer in 2 verschiedenen Subnetze mit einander an Pingt.
