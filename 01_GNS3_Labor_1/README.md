# Dokumentation Labor 1 - Ping mit Switch

 - Datum: 13.12.2021
 - Name: Robel Aysheshim
 - [Link zur Aufgabenstellung](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/07_GNS3%20Labor%20Anforderungen)



## Umgebung
![Umgebung](Images/Screen_6.png)


## Config VPC 1
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] 
ip 192.168.1.150 255.255.255.0 
```
## Config VPC 2
- [GNS3 VPCS](https://docs.gns3.com/docs/emulators/vpcs/)
- 1x Ethernet Interface
```
set pcname PC1
# [IP] [MASKE] 
ip 192.168.1.160 255.255.255.0 
```
## IP-Adresse Setzen
- Als erstes setzt man die IP-Adresse von den VPC's fest. Dies macht man in dem man auf das Config File geht. Ist man im Configfile gibt man dann folgendes ein."ip 192.168.1.150 255.255.255.0". Hier habe ich jetzt die IP-Adresse und Subnetzmaske festgesetzt.
- Ins Configfile kommt man indem man einen rechts Klick auf einen der beiden VPC's macht und dann auf "Edit Config" klickt. (Siehe unten in den beiden Hyperlinks)
- [configfile](Images/Screen_3.png)
- [IP-Adresse setzen](Images/Screen_4.png)

## Ping zwischen den VPC's
- Einen PC pingt man an indem man in der Commando Zeile "ping <IP-Adresse Zielgerät>" eingibt. Wenn alles richtig eingestellt worden ist (Netzwerk technisch) erhält man einen Ping reply vom Zielgerät. (Siehe Hyperlink)
- In meinem Fall habe ich "Ping 192.168.1.150" und "Ping 192.168.1.160"eigegeben. Somit habe ich beide Geräte angepingt. Nach dem dies Funktionierte war ich mit dem Labor 1 Auftrag fertig.
- [Ping reply VPC1](Images/Screen_5.png)
- [Ping reply VPC2](Images/Screen_7.png)
## Neue Lerninhalte
- Setzen einer IP-Adresse für VPC im GNS3
- Funktionen eines Switches im GNS3

## Reflexion
Mithilfe der Übung habe ich nun das Gefühl eine grobe Übersicht über GNS3 zu haben. Mit dieser Aufgabe bin ich nun definitiv einen Schritt näher gekommen mit dem GNS3 gut aus zu kommen. Ich fand die Übung sehr einfach und hatte keine grosse Mühe. Mir Persönlich hat mir dieser Labor Aufrag auch sehr gefallen. 
